package ru.jamakasi;

/**
 *
 * @author jamakasi
 */
public class Main {
    int worldx,worldy,lenght,frog,sleep;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String helpMessage = 
         "*************ERROR******************\n"+
         "*** -worldx size of the world M  ***\n"+
         "*** -worldy size of the world N  ***\n"+
         "*** -lenght snake lenght         ***\n"+
         "*** -frog frogs in world         ***\n"+
         "*** -sleep snake sleep time      ***\n"+
         "************************************\n";
        if(args==null || args.length<=0){
            System.out.println(helpMessage);
        }
        Main m =new Main();
        m.parseArgs(args);
        m.checkParams();
        m.startGame();
        
        
    }
    private void startGame(){
        GameWindow window = new GameWindow(worldx,worldy,frog,lenght,sleep);
        window.setVisible(true);
    }
    
    private void checkParams(){
        if(worldx <5){
            System.out.println("worldx require bigger than 5, used default 15");
            worldx=19;
        }
        if(worldy <5){
            System.out.println("worldy require bigger than 5, used default 15");
            worldy=15;
        }
        if(lenght <1){
            System.out.println("lenght require bigger or equals 1, used default 1");
            lenght=1;
        }
        if(frog <3){
            System.out.println("frog require bigger or equals 3, used default 3");
            frog=3;
            
        }
        if(sleep <1000){
            System.out.println("sleep require bigger or equals 1000, used default 1000");
            sleep=1000;
        }
    }
    private void parseArgs(String[] args){
        for(int i=0;i<=args.length-1;i++){
            if(args[i].equalsIgnoreCase("-worldx")){
                Integer num =null;
                if(i+1<=args.length-1) num= getInteger(args[i+1]);
                if(num!=null){
                    worldx=num;
                }else{
                    System.out.println("arg worldx require integer, used default 15");
                    worldx=15;
                }
            }else if(args[i].equalsIgnoreCase("-worldy")){
                Integer num =null;
                if(i+1<=args.length-1) num = getInteger(args[i+1]);
                if(num!=null){
                    worldy=num;
                }else{
                    System.out.println("arg worldy require integer, used default 15");
                    worldy=15;
                }
            }else if(args[i].equalsIgnoreCase("-lenght")){
                Integer num =null;
                if(i+1<=args.length-1) num = getInteger(args[i+1]);
                if(num!=null){
                    lenght=num;
                }else{
                    System.out.println("arg lenght require integer, used default 1");
                    lenght=1;
                }
            }else if(args[i].equalsIgnoreCase("-frog")){
                Integer num =null;
                if(i+1<=args.length-1) num = getInteger(args[i+1]);
                if(num!=null){
                    frog=num;
                }else{
                    System.out.println("arg frog require integer, used default 5");
                    frog=5;
                }
            }else if(args[i].equalsIgnoreCase("-sleep")){
                Integer num =null;
                if(i+1<=args.length-1) num = getInteger(args[i+1]);
                if(num!=null){
                    sleep=num;
                }else{
                    System.out.println("arg sleep require integer in msec, used default 1000");
                    sleep=1000;
                }
            }
        }
    }
    private static Integer getInteger(String str){
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return null;
    }
}
}
