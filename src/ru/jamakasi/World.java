package ru.jamakasi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import ru.jamakasi.gameObjects.Frog;
import ru.jamakasi.gameObjects.GameObject;
import ru.jamakasi.gameObjects.Snake;

/**
 *
 * @author jamakasi
 */
public class World {
    private final GameObjectTypes[][] world;
    private final int sizeX,sizeY;
    private final int frogCount,snakeLenght,snakeSleepTime;
    
    public AtomicInteger score;
    
    private Snake player;
    public List<GameObject> gameObjects = new ArrayList<GameObject>();

    public World(int sizeX, int sizeY,int frogCount,int snakeLenght,int snakeSleepTime) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.frogCount = frogCount;
        this.snakeLenght = snakeLenght;
        this.snakeSleepTime = snakeSleepTime;
        this.score = new AtomicInteger(0);
        this.world = new GameObjectTypes[sizeX][sizeY];
        resetWorld();
        
    }
    
    public void stopWorld(){
        for(GameObject obj:gameObjects){
            obj.die();
        }
        gameObjects.clear();
    }
    
    public void resetWorld(){
        stopWorld();
        for(int m=0;m<=sizeX-1;m++){
            for(int n=0;n<=sizeY-1;n++){
                world[m][n]=GameObjectTypes.EMPTY;
            }
        }
        score.set(0);
    }
    public void startNewGame(){
        resetWorld();
        player = new Snake(this,snakeLenght,snakeSleepTime);
        
        gameObjects.add(player);
        for(int i=0;i<=frogCount;i++){
            gameObjects.add(new Frog(this,snakeSleepTime*2));
        }
        
        for(GameObject obj:gameObjects){
            obj.spawn();
        }
        
    }

    public Snake getPlayer(){
        return player;
    }
    
    public GameObjectTypes[][] getWorld() {
        return world;
    }

    public GameObjectTypes getGameObject(int x, int y){
        return world[x][y];
    }
    
    public void setGameObject(int x,int y ,GameObjectTypes gameObject){
        world[x][y]=gameObject;
    }
    
    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }
    
    public enum GameObjectTypes{
        EMPTY,
        SNAKE_HEAD,
        SNAKE_BODY,
        SNAKE_HWOST,
        FROG
    }
    
    public void dump(){
        for(int x=0;x<=this.sizeX-1;x++){
            for(int y=0;y<=this.sizeY-1;y++){
                System.err.print("|"+this.world[x][y]);
            }
            System.err.println("|");
        }
    }
}
