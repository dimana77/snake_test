package ru.jamakasi.gameObjects;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import ru.jamakasi.World;

/**
 *
 * @author diman
 */
public class Frog extends GameObject{
    
    Random rnd;
    final List<DIRECTIONS> DIR_LIST = Collections.unmodifiableList(Arrays.asList(DIRECTIONS.values()));
    final int DIR_LIST_SIZE = DIR_LIST.size();
    
    public Frog(World world,int sleepTime) {
        super(0, 0, World.GameObjectTypes.FROG, world,sleepTime);
        rnd = new Random();
        findFreeCell();
        dir = DIRECTIONS.DOWN;
        thread.setName("Frog thread: "+UUID.randomUUID().toString());
    }
    
    private void findFreeCell(){
        
        boolean stop = false;
        int x,y;
        while(!stop){
            x = rnd.nextInt(world.getSizeX()-1);
            y = rnd.nextInt(world.getSizeY()-1);
            if(!hasWallCollision(x, y) && !hasObjCollision(x, y)){
                this.x = x;
                this.y = y;
                stop=true;
            }
        }
    }
    private void findFreeDirection(){
        if(tryMove()) return;
        for(int i = 0;i<=3;i++){
              dir = DIR_LIST.get(rnd.nextInt(DIR_LIST_SIZE));
              if(tryMove()) return;
        }
    }
    
    private boolean tryMove(){
        switch(dir){
            case UP:{
                if(!hasCollision(x-1, y)){
                    move(x-1, y);
                    return true;
                }
                return false;
            }
            case DOWN:{
                if(!hasCollision(x+1, y)){
                    move(x+1, y);
                    return true;
                }
                return false;
            }
            case LEFT:{
                if(!hasCollision(x, y-1)){
                    move(x, y-1);
                    return true;
                }
                return false;
            }
            case RIGHT:{
                if(!hasCollision(x, y+1)){
                    move(x, y+1);
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    
    @Override
    public void tick() {
        synchronized(world){
            findFreeDirection();
        }
    }

    @Override
    public void die() {
        super.die();
        
    }
    
    
    private boolean hasCollision(int x, int y){
        if(hasWallCollision(x, y)) return true;
        return hasObjCollision(x, y);
    }
    
    private void move(int newX,int newY){
        world.setGameObject(x, y, World.GameObjectTypes.EMPTY);
        x= newX;
        y= newY;
        world.setGameObject(newX, newY, World.GameObjectTypes.FROG);
    }
}
