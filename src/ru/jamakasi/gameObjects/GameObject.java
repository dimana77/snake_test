package ru.jamakasi.gameObjects;

import java.util.logging.Level;
import java.util.logging.Logger;
import ru.jamakasi.World;
import ru.jamakasi.World.GameObjectTypes;


/**
 *
 * @author jamakasi
 */
public abstract class GameObject implements Runnable{

    public enum DIRECTIONS{
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
    
    int x,y;
    final GameObjectTypes type;
    World world;
    DIRECTIONS dir;
    volatile boolean isDie=false;
    int sleepTime;
    
    Thread thread;
    
    /**
     * 
     * @param startX start position X
     * @param startY start position Y
     * @param type game object type
     * @param world 
     * @param sleepTime time to sleep thread
     */
    public GameObject(int startX,int startY,GameObjectTypes type,World world,int sleepTime) {
        this.x = startX; 
        this.y = startY;
        this.type = type;
        this.world = world;
        this.sleepTime = sleepTime;
        thread = new Thread(this);
    }

    public void spawn(){
        thread.start();
    }
    
    @Override
    public void run(){
        try {    
            while(!isDie){
                tick();
                Thread.sleep(sleepTime);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Snake.class.getName()).log(Level.SEVERE, "interrupted {0}", Thread.currentThread().getName());
            isDie = true;
        }
    }
    /**
     * all game logick here
     */
    public abstract void tick();
    
    /**
     * stop thread and stop ticks
     */
    public void die(){
        isDie=true;
        if(!thread.isInterrupted())thread.interrupt();
    }
    
    /**
     * 
     * @param x
     * @param y
     * @return true if cell not EMPTY
     */
    public boolean hasObjCollision(int x, int y){
        GameObjectTypes obj = world.getGameObject(x, y);
        if(obj!=GameObjectTypes.EMPTY) return true;
        return false;
    }
    
    /**
     *  Test cell
     * @param x
     * @param y
     * @return true if cell is not in world
     */
    public boolean hasWallCollision(int x, int y){
        if(x<0) return true;
        if(x>world.getSizeX()-1) return true;
        if(y<0) return true;
        if(y>world.getSizeY()-1) return true;
        return false;
    }
    
    public class Point{
        int x,y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Point{" + "x=" + x + ", y=" + y + '}';
        }
        
    }
}
