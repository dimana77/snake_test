package ru.jamakasi.gameObjects;


import java.util.Stack;
import java.util.UUID;
import ru.jamakasi.World;
import ru.jamakasi.World.GameObjectTypes;

/**
 *
 * @author jamakasi
 */
public class Snake extends GameObject{
    
    //ArrayList<Point> points;
    DIRECTIONS dir;
    Stack<Point> points = new Stack();
    
    public Snake(World world,int snakeLenght,int sleepTime) {
        super(0, 0, GameObjectTypes.SNAKE_BODY,world,sleepTime);
        
        isDie=false;
        for(int i=0;i<=snakeLenght-1;i++){
            points.add(new Point(0, i));
        }
        x=points.lastElement().x;
        y=points.lastElement().y;
        drawSnake();
        dir = DIRECTIONS.RIGHT;
        thread.setName("snake Thread: "+UUID.randomUUID().toString());
    }

    @Override
    public void tick() {
        synchronized(world){
            switch(dir){
                case UP:{
                    if(!hasCollision(x-1, y)){
                        moveTo(x-1, y);
                    }else if(hasWallCollision(x-1, y) || hasSnakeCollision(x-1, y)){
                        //game over
                        System.err.println("GameOver");
                        world.stopWorld();
                    }else if(hasObjCollision(x-1, y)){
                        //score++ snake lengh++
                        for(GameObject obj:world.gameObjects){
                            if(obj instanceof Frog){
                                Frog frog = (Frog) obj;
                                if(frog.x == x-1 && frog.y == y) {
                                    frog.die();
                                    eat(x-1, y);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
                case DOWN:{
                    if(!hasCollision(x+1, y)){
                        moveTo(x+1, y);
                    }else if(hasWallCollision(x+1, y) || hasSnakeCollision(x+1, y)){
                        //game over
                        System.err.println("GameOver");
                        world.stopWorld();
                    }else if(hasObjCollision(x+1, y)){
                        //score++ snake lengh++
                        for(GameObject obj:world.gameObjects){
                            if(obj instanceof Frog){
                                Frog frog = (Frog) obj;
                                if(frog.x == x+1 && frog.y == y) {
                                    frog.die();
                                    eat(x+1, y);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
                case LEFT:{
                    if(!hasCollision(x, y-1)){
                        moveTo(x, y-1);
                    }else if(hasWallCollision(x, y-1) || hasSnakeCollision(x, y-1)){
                        //game over
                        System.err.println("GameOver");
                        world.stopWorld();
                    }else if(hasObjCollision(x, y-1)){
                        //score++ snake lengh++
                        for(GameObject obj:world.gameObjects){
                            if(obj instanceof Frog){
                                Frog frog = (Frog) obj;
                                if(frog.x == x && frog.y == y-1) {
                                    frog.die();
                                    eat(x, y-1);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
                case RIGHT:{

                    if(!hasCollision(x, y+1)){
                        moveTo(x, y+1);
                    }else if(hasWallCollision(x, y+1) || hasSnakeCollision(x, y+1)){
                        //game over
                        System.err.println("GameOver");
                        world.stopWorld();
                    }else if(hasObjCollision(x, y+1)){
                        for(GameObject obj:world.gameObjects){
                            if(obj instanceof Frog){
                                Frog frog = (Frog) obj;
                                if(frog.x == x && frog.y == y+1){
                                    frog.die();
                                    eat(x, y+1);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    
    private void moveTo(int newX,int newY){
        Point last = points.get(0);
        world.setGameObject(last.x, last.y, GameObjectTypes.EMPTY);
        points.remove(0);
        last.x = newX;
        last.y = newY;
        x= newX;
        y= newY;
        points.add(last);
        drawSnake();
    }
    
    private void eat(int newX,int newY){
        x= newX;
        y= newY;
        points.add(new Point(newX, newY));
        drawSnake();
        world.score.set(world.score.get()+1);
    }
    
    private void drawSnake(){
        for(Point point:points){
            world.setGameObject(point.x, point.y, GameObjectTypes.SNAKE_BODY);
        }
        world.setGameObject(points.firstElement().x, points.firstElement().y, GameObjectTypes.SNAKE_HWOST);
        world.setGameObject(points.lastElement().x, points.lastElement().y, GameObjectTypes.SNAKE_HEAD);
    }
    
    private boolean hasCollision(int x, int y){
        if(hasWallCollision(x, y)) return true;
        if(hasSnakeCollision(x, y)) return true;
        return hasObjCollision(x, y);
    }
    
    private boolean hasSnakeCollision(int nx, int ny){
        GameObjectTypes obj = world.getGameObject(nx, ny);
        if(obj==GameObjectTypes.SNAKE_BODY ||
                obj==GameObjectTypes.SNAKE_HWOST || 
                obj==GameObjectTypes.SNAKE_HEAD) return true;
        return false;
    }
    
    public void turnTo(DIRECTIONS direction){
        dir = direction;
    }
    public void turnClock(){
        switch(dir){
            case UP: dir=DIRECTIONS.RIGHT; break;
            case RIGHT: dir=DIRECTIONS.DOWN; break;
            case DOWN: dir=DIRECTIONS.LEFT; break;
            case LEFT: dir=DIRECTIONS.UP; break;
        }
    }
    public void turnReverseClock(){
        switch(dir){
            case UP: dir=DIRECTIONS.LEFT; break;
            case RIGHT: dir=DIRECTIONS.UP; break;
            case DOWN: dir=DIRECTIONS.RIGHT; break;
            case LEFT: dir=DIRECTIONS.DOWN; break;
        }
    }
    
}
