package ru.jamakasi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ru.jamakasi.World.GameObjectTypes;

/**
 *
 * @author jamakasi
 */
public class GameWindow  extends JFrame{
    private World world;
    private final int cellSize;
    public GameWindow(int M,int N,int frogCount,int snakeLenght,int snakeSleepTime) {
        super("Snake game");
        this.setBounds(0, 0, 640, 480);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //int M = 10;
        //int N = 10;
        cellSize = 20;
        //int frogCount = 10;
        world = new World(M, N,frogCount,snakeLenght,snakeSleepTime);
        
        
        Container root = this.getContentPane();
        root.setLayout(new BorderLayout());
        
        final JButton startButton = new JButton("Start");
        final JButton stopButton = new JButton("Stop");
        stopButton.setEnabled(false);
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startButton.setEnabled(false);
                world.startNewGame();
                stopButton.setEnabled(true);
            }
        });
        
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startButton.setEnabled(true);
                world.stopWorld();
                stopButton.setEnabled(false);
            }
        });
        JLabel scoreLabel = new JLabel();
        Container top = new Container();
            top.setLayout(new FlowLayout(FlowLayout.LEFT));
            top.add(startButton);
            top.add(stopButton);
            top.add(scoreLabel);
        root.add(top, BorderLayout.NORTH);
        
        
        GameDisplay display = new GameDisplay(cellSize,world,scoreLabel);
        display.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1){
                    if(world.getPlayer()!=null)
                    world.getPlayer().turnClock();
                }
                if(e.getButton() == MouseEvent.BUTTON3){
                    if(world.getPlayer()!=null)
                    world.getPlayer().turnReverseClock();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
            }
        });
        
        root.add(display,BorderLayout.CENTER);
        pack();
        this.setMinimumSize(this.getPreferredSize());
    }
    
    class GameDisplay extends JPanel{

        private final int rowCount;
        private final int colCount;
        
        private final int cellWidth;
        private final int cellHeight;
        
        private final World world;
        private Graphics2D g2d;
        
        public GameDisplay(int cellSize,final World world,final JLabel scoreLabel) {
            this.rowCount = world.getSizeX();
            this.colCount = world.getSizeY();
            this.cellHeight = this.cellWidth = cellSize;
            this.world = world;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while(!Thread.interrupted()){
                            repaint();
                            scoreLabel.setText(String.format("Score: %d  ",world.score.get()));
                            Thread.sleep(100);
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(GameWindow.class.getName()).log(Level.SEVERE, "interrupted {0}", Thread.currentThread().getName());
                    }
                }
            }, "Thread g2d updater").start();
            this.setPreferredSize(new Dimension(rowCount*cellSize, colCount*cellSize));
        }

        
        
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g2d = (Graphics2D) g.create();

            drawCellsBuffer();
        }
        
        /**
         * Draw all cells
         */
        private void drawCellsBuffer(){
            synchronized(world){
                GameObjectTypes type = GameObjectTypes.EMPTY;
                for(int x=0;x<=colCount-1;x++){
                    int yOffset = x*cellWidth;

                    for(int y=0;y<=rowCount-1;y++){
                        int xOffset = y*cellHeight;
                        type = world.getGameObject(y, x);

                        switch(type){
                            case EMPTY : drawCell(xOffset, yOffset); break;
                            case SNAKE_HEAD : drawCellWithOval(xOffset, yOffset, cellWidth/2, Color.YELLOW); break;
                            case SNAKE_BODY : drawCellWithOval(xOffset, yOffset, cellWidth/3, Color.yellow); break;
                            case SNAKE_HWOST : drawCellWithOval(xOffset, yOffset, cellWidth/4, Color.yellow); break;
                            case FROG : drawCellWithOval(xOffset, yOffset, cellWidth/3, Color.GREEN); break;

                            //unknown
                            default:{
                                drawCell(xOffset, yOffset); break;
                            }
                        }
                    }
                }
                //world.dump();
            }
                
        }
        /**
         * Draw empty cell
         * @param x coord
         * @param y coord
         */
        private void drawCell(int x,int y){
            g2d.setColor(Color.black);
            g2d.draw(new Rectangle(x, y, cellWidth, cellHeight));
            g2d.setColor(this.getBackground());
            g2d.draw(new Rectangle(x+1, y+1, cellWidth-1, cellHeight-1));
        }
        /**
         * Draw foregroud oval
         * @param x coord
         * @param y coord
         * @param rad - oval radius
         * @param color oval color
         */
        private void drawCellWithOval(int x, int y, int rad, Color color){
            drawCell(x, y);
            g2d.setColor(color);
            g2d.fillOval(x+cellWidth/2-rad, y+cellHeight/2-rad, rad*2, rad*2);
        }
    }
}
